# t00t3rm

Dumb little script to display a continuous stream of toots.

Depends on:

* toot: https://toot.readthedocs.io/en/latest/index.html
* justify: https://tildegit.org/jns/justify

Best results on physical terminals with slow-scrolling feature ;)



