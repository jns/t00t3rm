#!/usr/bin/env bash

function grabtoot() {
  echo
  toot timeline --public --count 1 --once --no-color | tr -cd '[:print:]\n\t' |\
      grep -v '^ID' |\
      grep -v -P '^\s+\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}\s.*' |\
      grep -P -v '\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}$' |\
      grep -v '^$' 
      echo
}

while (true); do
    grabtoot | justify -w 80 -a center 2>/dev/null
    sleep 4
done
